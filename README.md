I have built this application with FastAPI as backend and MongoDB as DataBase
The reason for MongoDB is because we have to store the stream of values for each currency

This is the first time I'm working with MongoDB

To run the application, clone the repository & run `docker-compose up`

The Third-Party API used to build this application was CoinCap (https://docs.coincap.io/)
This API has almost all the features. But I've not used since we need to do the processing & storing in our own DB

I've used websockets from this API to stream the data directly from the API to the database.
(So, we need to wait for some time atleast for the data to get populated before performing any operation for the first time)

I've used mongoengine to query the database as an ORM

Since this is a test, I've kept all the ENVs in the docker files only.

The API docs could be obtianed at http://localhost:8008/docs
which has all the endpoints required.

P.S: I've build this solution without over engineering and kept it minimal as this is just a test. 
