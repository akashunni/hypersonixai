# 
FROM python:3.10.8

# 
WORKDIR /crypto

# 
COPY ./requirements.txt /crypto/requirements.txt

# 
RUN pip install --no-cache-dir --upgrade -r /crypto/requirements.txt

# 
COPY ./app /crypto/app

# 
# CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
