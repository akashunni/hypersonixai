from datetime import datetime
from fastapi import FastAPI, WebSocket, Query
import websockets
from mongoengine import connect
from mongoengine.errors import NotUniqueError
from pydantic import BaseModel

from app.services import services, coinwatch_socket
from app.db import models
from app.constants import CURRENCIES_MONITORED
from app.crypto_logger import logger

app = FastAPI()

# DB connection
connect(db='crypto', host='db', port=27017)

# Populating the 4 currencies to DB
for each_currency in CURRENCIES_MONITORED:
    currency = models.Currencies(name=each_currency)
    try:
        currency.save()
    except NotUniqueError as e:
        print(f"Ignoring {each_currency} as it already exists")


# This command fetches the currency values through websockets
coinwatch_socket.fetch_currencies()



class UserDetails(BaseModel):
    """
    Interface for user details
    """
    email: str
    currency: float
    price_range_min: float
    price_range_max: float




@app.get("/currencies")
def get_currencies():
    return services.get_active_currencies()


@app.get("/currencies/{currency}")
def get_current_value_of(currency):
    return services.get_current_data(currency)


@app.get("/all_currency_price_latest")
def all_currency_price_latest():
    return services.get_current_data_all_currencies()


@app.get("/get_min_max_values/{currency}")
def get_min_max_values(
        currency: str,
        selected_date: str = Query(default=datetime.today().strftime("%d-%m-%Y"))
    ):
    return services.get_min_max_currency(currency, selected_date)


@app.get("/get_prices_for_date/{currency}")
def get_prices_for_date(
        currency: str,
        selected_date: str = Query(default=datetime.today().strftime("%d-%m-%Y")),
        offset: int = Query(default=0, ge=0),
        limit: int = Query(default=5, ge=0, le=100)
    ):
    return services.get_prices_date(currency, selected_date, offset, limit)


@app.post("/add_user")
def add_user(user_details: UserDetails):
    user_details_dict = user_details.dict()
    return services.add_user_details(
        user_email=user_details.email,
        currency=user_details.currency, 
        min_value=user_details.price_range_min,
        max_value=user_details.price_range_max
    )
