from mongoengine import Document, StringField, DateTimeField, FloatField, ReferenceField
import datetime


class Currencies(Document):
    """
    Currency Model to store the monitored currencies
    """
    name = StringField(max_length=20, required=True, unique=True)
    date_modified = DateTimeField(default=datetime.datetime.utcnow)


class Prices(Document):
    """
    Price Model to store the live feed of values from the monitored currencies
    """
    currency = StringField(max_length=20, required=True)
    price_usd = FloatField()
    date_modified = DateTimeField(default=datetime.datetime.utcnow)


class Users(Document):
    """
    User Model to store the user details
    """
    email = StringField(max_length=100, required=True)
    currency = FloatField(required=True)
    price_range_min = FloatField(default=0.0)
    price_range_max = FloatField(default=10.0)
