import websockets
import asyncio
import json
from app.db import models
from app.constants import CURRENCIES_MONITORED

SOCKET_URI = "wss://ws.coincap.io/prices?assets=" + ",".join(CURRENCIES_MONITORED)

connection = websockets.connect(uri=SOCKET_URI)

async def check(): 
    async with connection as websocket:
        async for message in websocket:
            _dict = json.loads(message)
            for each_currency, value in _dict.items():
                price = models.Prices(currency=each_currency, price_usd=value)
                price.save()

        await websocket.close()


def fetch_currencies():
    loop = asyncio.get_event_loop()
    loop.create_task(check())