from datetime import datetime, timedelta
import requests
import websockets
import json
from ..db.models import Currencies, Prices, Users
from ..crypto_logger import logger


BASE_URL = "https://api.coincap.io/v2"


def get_active_currencies() -> list:
    logger.info("Fetching all monitored currencies")
    result = []
    for each_currency in Currencies.objects:
        result.append(each_currency.name)
    return result


def get_current_data(currency: str) -> dict:
    logger.info(f"Fetching latest data for {currency}")
    result = {}
    latest_value = Prices.objects(currency=currency).order_by(
        '-date_modified'
    ).only(
        'currency',
        'price_usd',
        'date_modified'
    ).first()
    
    return {
        'currency': latest_value.currency,
        'price_usd': latest_value.price_usd,
        'date_modified': latest_value.date_modified
    }


def get_current_data_all_currencies() -> list:
    logger.info(f"Fetching latest data for all currencies")
    result = []
    currency_list = get_active_currencies()
    for each_currency in currency_list:
        result.append(get_current_data(each_currency))
    return result


def get_min_max_currency(currency: str, selected_date: datetime) -> dict:
    logger.info(f"Fetching min & max data for currency for the day mentioned")
    result = {}

    selected_date_start = datetime.strptime(selected_date, "%d-%m-%Y").date()
    selected_date_end = selected_date_start + timedelta(days=1)

    min_value = Prices.objects(
        currency=currency,
        date_modified__gte=selected_date_start,
        date_modified__lte=selected_date_end
    ).order_by(
        '-price_usd'
    ).only(
        'price_usd'
    ).first()

    max_value = Prices.objects(
        currency=currency,
        date_modified__gte=selected_date_start,
        date_modified__lte=selected_date_end
    ).order_by(
        'price_usd'
    ).only(
        'price_usd'
    ).first()

    return {
        'currency': currency,
        'min_value': min_value.price_usd,
        'max_value': max_value.price_usd,
        'date': selected_date
    }


def get_prices_date(currency: str, selected_date: str, offset: int, limit: int) -> dict:
    logger.info(f"Fetching latest data for {currency}")
    result = {
        "currency": currency,
        "data": [],
        "offset": offset,
        "limit": limit
    }

    day_values = Prices.objects(currency=currency).order_by(
        '-date_modified'
    ).only(
        'price_usd',
        'date_modified'
    )[offset:offset + limit]
    
    for each_value in day_values:
        result["data"].append({
            'price_usd': each_value.price_usd,
            'date_modified': each_value.date_modified
        })
    
    return result

def add_user_details(user_email: str, currency: str, min_value: float, max_value: float) -> dict:
    logger.error(f"Adding user detail")
    user = Users(
        email=user_email,
        currency=currency,
        price_range_min=min_value,
        price_range_max=max_value
    )
    try:
        user.save()
        return json.loads(user.to_json())
    except Exception as e:
        logger.error(f"Error while adding a user")
        return "Failed"
